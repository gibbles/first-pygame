import pygame
import time
import random

pygame.init()

display_width = 800
display_height = 600

BLACK = (0,0,0)
WHITE = (255,255,255)
RED = (200,0,0)
BRIGHT_RED = (255,0,0)
GREEN = (0,200,0)
BRIGHT_GREEN = (0,255,0)
BLOCK_COLOR = (53,115,255)

TITLE = 'Avoidance Racer'

game_display = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption(TITLE)
clock = pygame.time.Clock()

car_image = pygame.image.load('resources/racecar.png')
CAR_WIDTH = 73

crash_sound = pygame.mixer.Sound('resources/Crash.wav')
pygame.mixer.music.load('resources/Sprightly_Pursuit.wav')

pygame.display.set_icon(car_image)

paused = False

def things_dodged(count):
    font = pygame.font.SysFont(None, 25)
    text = font.render("Dodged: " + str(count), True, BLACK)
    game_display.blit(text, (0,0))


def things(thingx, thingy, thingw, thingh, color):
    pygame.draw.rect(game_display, color, [thingx, thingy, thingw, thingh])


def car(x,y):
    game_display.blit(car_image, (x,y))


def text_objects(text, font):
    text_surface = font.render(text, True, BLACK)
    return text_surface, text_surface.get_rect()


def quit_game():
    pygame.quit()
    quit()


def button(msg, x, y, width, height, inactive_color, active_color, action = None):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x + width > mouse[0] > x and y + height > mouse[1] > y:
        pygame.draw.rect(game_display, active_color, (x, y, width, height))
        if click[0] == 1 and action != None:
            action()
    else:
        pygame.draw.rect(game_display, inactive_color, (x, y, width, height))
    
    small_text = pygame.font.Font('freesansbold.ttf', 20)
    text_surface, text_rectangle = text_objects(msg, small_text)
    text_rectangle.center = ((x + (width/2)), (y + (height/2)))
    game_display.blit(text_surface, text_rectangle)


def crash():
    pygame.mixer.Sound.play(crash_sound)
    pygame.mixer.music.stop()

    large_text = pygame.font.SysFont('comicsansms', 115)
    text_surface, text_rectangle = text_objects('You Crashed', large_text)
    text_rectangle.center = ((display_width/2), (display_height/2))
    game_display.blit(text_surface, text_rectangle)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()

        button('Try Again', 150, 450, 100, 50, GREEN, BRIGHT_GREEN, game_loop)
        button('Quit', 550, 450, 100, 50, RED, BRIGHT_RED, quit_game)

        pygame.display.update()
        clock.tick(15)


def game_intro():
    intro = True

    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()

        game_display.fill(WHITE)
        large_text = pygame.font.Font('freesansbold.ttf',75)
        text_surface, text_rectangle = text_objects(TITLE, large_text)
        text_rectangle.center = ((display_width/2), (display_height/2))
        game_display.blit(text_surface, text_rectangle)

        button('Go!', 150, 450, 100, 50, GREEN, BRIGHT_GREEN, game_loop)
        button('Quit', 550, 450, 100, 50, RED, BRIGHT_RED, quit_game)

        pygame.display.update()
        clock.tick(15)


def unpause():
    global paused
    pygame.mixer.music.unpause()
    paused = False


def pause():
    pygame.mixer.music.pause()

    large_text = pygame.font.SysFont('comicsansms', 115)
    text_surface, text_rectangle = text_objects('Paused', large_text)
    text_rectangle.center = ((display_width/2), (display_height/2))
    game_display.blit(text_surface, text_rectangle)

    global paused
    paused = True

    while paused:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()
        
        button('Continue', 150, 450, 100, 50, GREEN, BRIGHT_GREEN, unpause)
        button('Quit', 550, 450, 100, 50, RED, BRIGHT_RED, quit_game)

        pygame.display.update()
        clock.tick(15)


def game_loop():
    pygame.mixer.music.play(-1)

    x = (display_width * 0.45)
    y = (display_height * .8)

    x_change = 0

    thing_width = 100
    thing_height = 100
    thing_startx = random.randrange(0, display_width - thing_width)
    thing_starty = -600
    thing_speed = 4

    thing_count = 1
    dodged = 0

    game_exit = False

    while not game_exit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit_game()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_change = -5
                elif event.key == pygame.K_RIGHT:
                    x_change = 5
                elif event.key == pygame.K_p:
                    pause()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_change = 0
        
        x += x_change

        game_display.fill(WHITE)

        things(thing_startx, thing_starty, thing_width, thing_height, BLOCK_COLOR)
        thing_starty += thing_speed
        car(x,y)
        things_dodged(dodged)

        if x > display_width - CAR_WIDTH or x < 0:
            crash()

        if thing_starty > display_height:
            thing_starty = 0 - thing_height
            thing_startx = random.randrange(0, display_width - thing_width)
            dodged += 1
            thing_speed += 1
            thing_width += int(dodged * 1.2)

        if y < thing_starty + thing_height:
            if x > thing_startx and x < thing_startx + thing_width or x + CAR_WIDTH > thing_startx and x + CAR_WIDTH < thing_startx + thing_width:
                crash()

        pygame.display.update()
        clock.tick(60)

game_intro()
game_loop()
quit_game()
