import pygame

pygame.init()

WHITE = (255,255,255)
BLACK = (0,0,0)
RED = (255,0,0)
GREEN = (0,255,0)
BLUE = (0,0,255)

game_display = pygame.display.set_mode((800,600))
pygame.display.set_caption('Drawing stuff')

game_display.fill(BLACK)

pix = pygame.PixelArray(game_display)
pix[10][20] = GREEN

pygame.draw.line(game_display, BLUE, (100,200), (300,450), 5)

pygame.draw.rect(game_display, RED, (400,400,50,25))

pygame.draw.circle(game_display, WHITE, (150,150), 75)

pygame.draw.polygon(game_display, GREEN, ((25,75),(76,125),(250,375),(400,25),(60,540)))

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

    pygame.display.update()
